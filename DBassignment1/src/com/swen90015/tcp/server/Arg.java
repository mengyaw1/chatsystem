package com.swen90015.tcp.server;

import org.kohsuke.args4j.Option;

public class Arg {

	@Option(name = "-p", usage = "Determine the port")
	public Integer port = 4444;
}
