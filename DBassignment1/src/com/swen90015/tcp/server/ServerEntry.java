package com.swen90015.tcp.server;

import java.io.IOException;

import org.kohsuke.args4j.CmdLineException;
import org.kohsuke.args4j.CmdLineParser;

public class ServerEntry {

	public static void main(String[] args) throws IOException {
		// parse cmdLine input
		//long currenttime = System.currentTimeMillis();
		//System.out.println(currenttime);
		//String str_current = Long.toString(currenttime);
		//System.out.println(str_current);
		//long kkk = Long.parseLong(str_current);
		//System.out.println(kkk);
		//System.out.println(currenttime+kkk);
		//boolean a = false;
		//if ((currenttime+kkk)>currenttime){
		//	a = true;
		//	System.out.println(a);
		//}

		Arg arg = new Arg();
		CmdLineParser parser = new CmdLineParser(arg);
		try {
			parser.parseArgument(args);
		} catch (CmdLineException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		// new a thread to listen to client
		Thread server = new Thread(new Server(arg.port));
		server.start();

	}

}
