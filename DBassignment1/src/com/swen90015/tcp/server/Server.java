package com.swen90015.tcp.server;

import java.io.DataOutputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

import javax.net.ssl.KeyManager;
import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLServerSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.TrustManagerFactory;

import org.json.simple.JSONObject;

public class Server implements Runnable {
	// to keep all rooms clients information in this hash map
	private ConcurrentHashMap<String, Room> roomMap = new ConcurrentHashMap<String, Room>();
	private ServerSocket serverSocket = null;
	// to generate the identity for user
	private int id = 0;
	// tcp port
	private Integer port;
	// to store kick history

	private ConcurrentHashMap<String, List<String>> kickList = new ConcurrentHashMap<String, List<String>>();
	//private ConcurrentHashMap<String, String> authenticatedClient = new ConcurrentHashMap<String, String>();
	

	
	// to store authenticated users
	private ConcurrentHashMap<String, String> authClients = new ConcurrentHashMap<String, String>();

	// initialize the roomMap
	public Server(Integer port) {
		this.port = port;
		Room MainHall = new Room();
		MainHall.setRoomIdentity("MainHall");
		MainHall.setOwner(null);
		getRoomMap().put(MainHall.getRoomIdentity(), MainHall);
	}

	public void run() {			
		// Server is listening
		try {
			
			// hard code the values of the JVM variables as follows: 
//			if (new File("certificates/server/serverkeystore.jks").isFile()) {
//				System.setProperty("javax.net.ssl.keyStore","certificates/server/serverkeystore.jks");
//				System.setProperty("javax.net.ssl.keyStorePassword","password");	
//			}
			
			//Sun SSL
			char[] passphrase = "password".toCharArray();
		    KeyStore keystore = KeyStore.getInstance("JKS");
		    InputStream keystoreInputStream = getClass().getResourceAsStream("/serverkeystore.jks");
		    keystore.load(keystoreInputStream, passphrase);
//		    keystore.load(new FileInputStream("/clientkeystore.jks"), passphrase);
		    
		    KeyStore trustKeyStore = KeyStore.getInstance("JKS");
		    InputStream trustKeyStoreInputStream = getClass().getResourceAsStream("/servertruststore.jks");
		    trustKeyStore.load(trustKeyStoreInputStream, passphrase);
//		    trustKeyStore.load(new FileInputStream("/clienttruststore.jks"), passphrase);
		    
		    KeyManagerFactory kmf = KeyManagerFactory.getInstance("SunX509");
		    kmf.init(keystore,passphrase);
		    KeyManager[] keyManagers = kmf.getKeyManagers();

		    TrustManagerFactory tmf = TrustManagerFactory.getInstance("SunX509");
		    tmf.init(trustKeyStore);
		    TrustManager[] trustManagers = tmf.getTrustManagers();

		    SSLContext context = SSLContext.getInstance("TLS");
		    context.init(keyManagers, trustManagers, null);
			
			// Create SSL server socket factory, which creates SSLServerSocket instances
		    SSLServerSocketFactory factory = context.getServerSocketFactory();
		    serverSocket = factory.createServerSocket(port);
//			ServerSocketFactory factory = SSLServerSocketFactory.getDefault();
//			serverSocket = factory.createServerSocket(port);
			System.out.println("Server is listening...");

			while (true) {
				// Server waits for a new connection
				Socket socket = serverSocket.accept();
				// Java creates new socket object for each connection.

				System.out.println("Client Connected...");

				// A new thread is created per client
				Thread client = new Thread(new ClientThread(socket, this));
				// It starts running the thread by calling run() method
				client.start();

			}

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (CertificateException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (KeyStoreException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (UnrecoverableKeyException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (KeyManagementException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public String messageCentral(JSONObject object) {
		String type = (String) object.get("type");
		return type;
	}

	// to send message to all clients
	public void sendMessageToAll(JSONObject object) {
		Iterator<Room> rooms = getRoomMap().values().iterator();
		while (rooms.hasNext()) {
			Room room = rooms.next();
			if (room.getClients() != null) {
				Iterator<ClientThread> clients = room.getClients().values()
						.iterator();
				while (clients.hasNext()) {
					try {
						DataOutputStream out = new DataOutputStream(clients
								.next().getSocket().getOutputStream());
						out.writeUTF(object.toJSONString());
						out.flush();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}
		}

	}

	public String identityGenerate() {
		String identity = "guest" + id;
		id = id + 1;
		return identity;
	}

	public ConcurrentHashMap<String, Room> getRoomMap() {
		return roomMap;
	}

	public void setRoomMap(ConcurrentHashMap<String, Room> roomMap) {
		this.roomMap = roomMap;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public ConcurrentHashMap<String, List<String>> getKick() {
		return kickList;
	}

	public void setKick(ConcurrentHashMap<String, List<String>> kick) {
		this.kickList = kick;
	}

	public ConcurrentHashMap<String, String> getAuthClients() {
		return authClients;
	}

	public void setAuthClients(ConcurrentHashMap<String, String> authClients) {
		this.authClients = authClients;
	}

}
