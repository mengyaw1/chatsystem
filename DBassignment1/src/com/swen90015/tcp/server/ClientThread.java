package com.swen90015.tcp.server;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.EOFException;
import java.io.IOException;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map.Entry;
import java.util.concurrent.ConcurrentHashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

public class ClientThread implements Runnable {
	private Socket socket;
	private Server server;
	private DataOutputStream out;
	private String identity;
	private Room room;

	public ClientThread(Socket socket, Server server) {
		this.socket = socket;
		this.server = server;
		this.identity = "";
		this.setRoom(new Room());
		try {
			this.out = new DataOutputStream(socket.getOutputStream());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void run() {
		try {
			DataInputStream in = new DataInputStream(socket.getInputStream());
			EncodingServer decodingServer = new EncodingServer();

			// initialize a new client
			initClient();
			// move new client to MainHall
			initClientRoom();
			// send room content and to new client
			roomContent("MainHall");
			roomList();

			try {
				while (true) {
					// parse msg from cliet to JSONObject
					String msg = in.readUTF();
					JSONParser parser = new JSONParser();
					JSONObject fromClient = (JSONObject) parser.parse(msg);
					// get msg type and process differently
					String type = server.messageCentral(fromClient);
					switch (type) {
					case "identitychange":
						identityChange(decodingServer.identityChange(
								fromClient, identity,
								identityVerify(fromClient)));
						break;
					case "join":
						roomChange(decodingServer.roomChange(fromClient,
								identity, room, roomVerify(fromClient)));
						break;
					case "who":
						roomContent((String) fromClient.get("roomid"));
						break;
					case "list":
						roomList();
						break;
					case "createroom":
						createRoom(fromClient);
						break;
					case "kick":
						kick(fromClient);
						break;
					case "delete":
						deleteRoom(fromClient);
						break;
					case "message":
						message(fromClient);
						break;
					case "login":
						login(fromClient);
						break;
					case "register":
						register(fromClient);
						break;
					case "quit":
						roomChangeNull();
						break;
					}

				}
			} catch (EOFException e) {
				if (socket != null)
					socket.close();
				System.out.println("Client disconnected.");
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			// when client close socket server will process quit
			quit();

		} catch (IOException e) {
			e.printStackTrace();
		}

		// A thread finishes if run method finishes
	}

	/*
	 * new identity for new client
	 */
	@SuppressWarnings("unchecked")
	public void initClient() {
		// generate new identity JSONObject
		JSONObject newIdentity = new JSONObject();
		String identity = server.identityGenerate();
		newIdentity.put("type", "newidentity");
		newIdentity.put("former", "");
		newIdentity.put("identity", identity);
		// set identity for client
		setIdentity((String) newIdentity.get("identity"));
		// send the new identity to client
		sendMessage(newIdentity);
	}

	// move new client to MainHall
	@SuppressWarnings("unchecked")
	public void initClientRoom() {
		// add new client to MainHall
		server.getRoomMap().get("MainHall").getClients().put(identity, this);
		// set room for client
		setRoom(server.getRoomMap().get("MainHall"));
		// send room move message to client
		JSONObject roomMessage = new JSONObject();
		roomMessage.put("type", "roomchange");
		roomMessage.put("identity", identity);
		roomMessage.put("former", "");
		roomMessage.put("roomid", getRoom().getRoomIdentity());
		sendMessage(roomMessage);

	}

	// response to show the room content to client
	@SuppressWarnings("unchecked")
	public void roomContent(String roomIdentity) {
		if (roomIdentity != null) {
			JSONObject roomContent = new JSONObject();
			List<String> identities = new ArrayList<String>();
			if (!server.getRoomMap().get(roomIdentity).getClients().isEmpty()) {
				Iterator<Entry<String, ClientThread>> clients = server.getRoomMap()
						.get(roomIdentity).getClients().entrySet().iterator();
				while (clients.hasNext()) {
					Entry<String, ClientThread> pair = clients.next();
					if (pair.getKey().equals(
							server.getRoomMap().get(roomIdentity).getOwner())) {
						identities.add(pair.getKey() + "*");
					} else {
						identities.add(pair.getKey());
					}
				}
			}

			roomContent.put("type", "roomcontent");
			roomContent.put("roomid", roomIdentity);
			roomContent.put("identities", identities);
			roomContent.put("owner", server.getRoomMap().get(roomIdentity)
					.getOwner());

			sendMessage(roomContent);
		}
	}

	// show all room lists to client
	@SuppressWarnings("unchecked")
	public void roomList() {
		// the room list to return to client
		JSONObject roomList = new JSONObject();
		// put type to the room list
		roomList.put("type", "roomlist");
		// put rooms to the room list
		JSONArray roomsArray = new JSONArray();
		Iterator<Room> rooms = server.getRoomMap().values().iterator();
		while (rooms.hasNext()) {
			JSONObject objRoom = new JSONObject();
			Room room = rooms.next();
			objRoom.put("roomid", room.getRoomIdentity());
			objRoom.put("count", room.getClients().size());
			roomsArray.add(objRoom);
		}
		roomList.put("rooms", roomsArray);

		// send the room list to client
		sendMessage(roomList);
	}

	// verify if the new identity is used or not
	public boolean identityVerify(JSONObject object) {
		boolean used = false;
		Iterator<Room> rooms = server.getRoomMap().values().iterator();
		while (rooms.hasNext()) {
			// if there is client whose identity is this then set it as used
			if (rooms.next().getClients().get(object.get("identity")) != null) {
				used = true;
				break;
			}
		}
		if (server.getAuthClients().contains(object.get("identity"))){
			used = true;
		}
		
		if (server.getKick().contains(object.get("identity"))){
			used = true;
		}
		return used;
	}

	//login, to do identitychange
	@SuppressWarnings("unchecked")
	public void login(JSONObject object) {
		if (server.getAuthClients().containsKey(object.get("identity"))){
			if (server.getAuthClients().contains(object.get("password"))){
				
				JSONObject loginIdChange = new JSONObject();
				loginIdChange.put("type", "newidentity");
				loginIdChange.put("former", this.identity);
				loginIdChange.put("identity", object.get("identity"));
				server.getRoomMap().get(room.getRoomIdentity()).getClients().remove(identity);
				this.identity = object.get("identity").toString();
				server.getRoomMap().get(room.getRoomIdentity()).getClients().put(identity, this);
				
				server.sendMessageToAll(loginIdChange);
				
				
			}
			else {
				failToAuth(object);
			}
		}
		else {
			failToAuth(object);
		}
	}
	
	public void register(JSONObject object) {
		if (!identityVerify(object)){
			server.getAuthClients().put(object.get("identity").toString(), object.get("password").toString());
			login(object);
		}
		else {
			failToAuth(object);
		}
	}
	
	@SuppressWarnings("unchecked")
	public void failToAuth(JSONObject object) {
		JSONObject failToAuth = new JSONObject();
		failToAuth.put("type", "newidentity");
		failToAuth.put("former", this.identity);
		failToAuth.put("identity", this.identity);
		
		sendMessage(failToAuth);
		
	}
	
	// response to identity change request
	public void identityChange(JSONObject object) {

		// if the change is rejected then only send it to the requester
		if (object.get("identity").equals(object.get("former"))) {
			sendMessage(object);
		}
		// if the change is accepted
		else {
			//verify if user is authenticated
			if (server.getAuthClients().containsKey(object.get("former"))){
				String temp_password = server.getAuthClients().get(object.get("former"));
				server.getAuthClients().remove(object.get("former"));
				server.getAuthClients().put(object.get("identity").toString(), temp_password);
			}
			
			//verify if user is in kickList
			if (server.getKick().containsKey(object.get("former"))){
				List<String> temp_kick = server.getKick().get(object.get("former"));
				server.getKick().remove(object.get("former"));
				server.getKick().put(object.get("identity").toString(), temp_kick);
			}
			
			// remove old identity
			server.getRoomMap().get(room.getRoomIdentity()).getClients()
					.remove(identity);
			// change the identity of room owner
			Iterator<Room> rooms = server.getRoomMap().values().iterator();
			if (rooms.hasNext()) {
				Room room = rooms.next();
				if (!room.getRoomIdentity().equals("MainHall")) {
					if (room.getOwner().equals(identity)) {
						server.getRoomMap().get(room.getRoomIdentity())
								.setOwner((String) object.get("identity"));
					}
				}
			}

			// set new identity to this thread
			setIdentity((String) object.get("identity"));
			// set new identity for client
			server.getRoomMap().get(room.getRoomIdentity()).getClients()
					.put(identity, this);

			server.sendMessageToAll(object);
		}
	}

	// verify if the room exists
	public boolean roomVerify(JSONObject object) {
		boolean isValid = false;
		Iterator<Room> rooms = server.getRoomMap().values().iterator();
		while (rooms.hasNext()) {
			// if there is room whose identity is this then set it as existed
			if (rooms.next().getRoomIdentity().equals(object.get("roomid"))) {
				isValid = true;
				break;
			}
		}
		return isValid;
	}

	// respond to room change request
	@SuppressWarnings("unchecked")
	public void roomChange(JSONObject object) {
		JSONObject roomChange = object;
		//if kickList contains the current id
		if (server.getKick().containsKey(object.get("identity")) &&
				server.getKick().get(object.get("identity")).get(0).equals(object.get("roomid")) && 
					Long.parseLong(server.getKick().get(object.get("identity")).get(1)) > System.currentTimeMillis()){
			//if current id wanna change room to the room which kick him for some time
			//if (server.getKick().get(object.get("identity")).get(0).equals(object.get("roomid"))){
				//if within kick time for the current id for changing to the target room
				//if (Long.parseLong(server.getKick().get(object.get("identity")).get(1)) > System.currentTimeMillis()){
					//return a json to move id from current room to current room
					JSONObject failRoomChange = new JSONObject();
					failRoomChange.put("type", "roomchange");
					failRoomChange.put("identity", object.get("identity"));
					failRoomChange.put("former", object.get("former"));
					failRoomChange.put("roomid", object.get("former"));
					sendMessage(failRoomChange);
					return;
				//}
			//}
		}
		else {
			// if the change is rejected then only sent to requester
			if (roomChange.get("roomid").equals(roomChange.get("former"))) {
				sendMessage(roomChange);
			}
			// if the change is accepted
			else {
				// move the client to a new room
				server.getRoomMap().get(room.getRoomIdentity()).getClients()
						.remove(identity);
				server.getRoomMap().get(roomChange.get("roomid")).getClients()
						.put(identity, this);
				// set room for this thread
				setRoom(server.getRoomMap().get(roomChange.get("roomid")));
				// broadcast the message to all clients
				server.sendMessageToAll(roomChange);
				// if the client is moved to MainHall, send room content and room
				// list to all clients in MainHall
				if (roomChange.get("roomid").equals("MainHall")) {
					roomContent("MainHall");
					roomList();
				}
			}
		}

		
	}

	// respond to create room request
	public void createRoom(JSONObject object) {
		// verify if the room identity is valid expression
		String reg = "[A-Za-z][0-9A-Za-z]{2,31}";
		Pattern pattern = Pattern.compile(reg);
		Matcher matcher = pattern.matcher((String) object.get("roomid"));
		boolean isValid = matcher.matches();
		// verify that room not exists and the expression is valid
		if (!roomVerify(object) && isValid) {
			Room room = new Room();
			room.setRoomIdentity((String) object.get("roomid"));
			room.setOwner(identity);
			// create new room successfully
			server.getRoomMap().put(room.getRoomIdentity(), room);
			roomList();
		}
		// fails to create new room
		else {
			roomListNull();
		}
	}

	// a null room list to respond to failed room create
	@SuppressWarnings("unchecked")
	public JSONObject roomListNull() {
		// the room list to return to client
		JSONObject roomList = new JSONObject();
		// put type to the room list
		roomList.put("type", "roomlist");
		// put null rooms
		roomList.put("rooms", null);

		// send the room list to client
		sendMessage(roomList);
		return roomList;
	}

	// respond to delete room request
	@SuppressWarnings("unchecked")
	public void deleteRoom(JSONObject object) {
		// check if the client is the room owner
		if (server.getRoomMap().get(object.get("roomid")).getOwner()
				.equals(identity)) {
			// if the room is not empty
			if (server.getRoomMap().get(object.get("roomid")).getClients() != null) {
				Iterator<Entry<String, ClientThread>> clients = server.getRoomMap()
						.get(object.get("roomid")).getClients().entrySet()
						.iterator();
				while (clients.hasNext()) {
					// get the next client in the delete room
					Entry<String, ClientThread> pair = clients.next();
					// move the next client to MainHall
					server.getRoomMap().get("MainHall").getClients()
							.put(pair.getKey(), pair.getValue());
					//set room for next client thread
					pair.getValue().setRoom(server.getRoomMap().get("MainHall"));
					// create a room change response to the next client in the
					// delete room
					JSONObject roomChange = new JSONObject();
					roomChange.put("type", "roomchange");
					roomChange.put("identity", pair.getKey());
					roomChange.put("former", object.get("roomid"));
					roomChange.put("roomid", "MainHall");
					// send the room change response to next client
					try {
						DataOutputStream out = new DataOutputStream(pair
								.getValue().getSocket().getOutputStream());
						out.writeUTF(roomChange.toJSONString());
						out.flush();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

				}
			}
			// delete the room
			server.getRoomMap().remove(object.get("roomid"));
			roomList();
		}

	}

	public void kick(JSONObject object) {
		Long currentTime = System.currentTimeMillis();
		List<String> kickUser = new ArrayList<String>();
		kickUser.add(object.get("roomid").toString());
		long endKick = currentTime + Long.parseLong(object.get("time").toString())*1000L;
		kickUser.add(Long.toString(endKick));
		
		if (server.getRoomMap().containsKey(object.get("roomid")) && 
				server.getRoomMap().get(object.get("roomid")).getClients().containsKey(object.get("identity"))){
			
			server.getKick().put(server.getRoomMap().get(object.get("roomid")).getClients().get(object.get("identity")).getIdentity(), kickUser);
			// if the room exists
			if (server.getRoomMap().get(object.get("roomid")) != null) {
				// verify if the requester is the owner of the room
				if (server.getRoomMap().get(object.get("roomid")).getOwner()
						.equals(identity)) {
					// get the socket of the kicked client
					ClientThread client = server.getRoomMap().get(object.get("roomid"))
							.getClients().get(object.get("identity"));
					// verify the kicked client is in this room
					if (client != null && client.getSocket() != socket) {
						// remove the kicked client from current room
						server.getRoomMap().get(object.get("roomid")).getClients()
								.remove(object.get("identity"));
						// add the kicked client to MainHall
						server.getRoomMap().get("MainHall").getClients()
								.put((String) object.get("identity"), client);
						// set room for the kicked client thread
						client.setRoom(server.getRoomMap().get("MainHall"));
						// send room change message
						kickRoomChange(object);
					}
				}
			}
		}
	}

	@SuppressWarnings("unchecked")
	public void kickRoomChange(JSONObject object) {
		// create an appropriate room change message
		JSONObject roomChange = new JSONObject();
		roomChange.put("type", "roomchange");
		roomChange.put("identity", object.get("identity"));
		roomChange.put("former", object.get("roomid"));
		roomChange.put("roomid", "MainHall");
		server.sendMessageToAll(roomChange);
	}

	// respond to normal message request
	@SuppressWarnings("unchecked")
	public void message(JSONObject object) {
		JSONObject message = object;
		message.put("identity", identity);
		// if the room is not empty
		if (server.getRoomMap().get(room.getRoomIdentity()).getClients() != null) {
			Iterator<ClientThread> clients = server.getRoomMap()
					.get(room.getRoomIdentity()).getClients().values()
					.iterator();
			// send the message to all clients in this room
			while (clients.hasNext()) {
				try {
					ClientThread client = clients.next();
					DataOutputStream out = new DataOutputStream(
							client.getSocket().getOutputStream());
					out.writeUTF(message.toJSONString());
					out.flush();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			}
		}
	}

	// when client request quit server treat it as a room change request to
	// empty room
	@SuppressWarnings("unchecked")
	public void roomChangeNull() {
		// create an appropriate room change message
		JSONObject roomChange = new JSONObject();
		roomChange.put("type", "roomchange");
		roomChange.put("identity", identity);
		roomChange.put("former", room.getRoomIdentity());
		roomChange.put("roomid", "");
		sendMessage(roomChange);
	}

	// disconnect client
	public void quit() {
		// remove the client from current room
		server.getRoomMap().get(room.getRoomIdentity()).getClients().remove(identity);
		if (!server.getAuthClients().containsKey(identity)){
			// set room owner to empty
			if (server.getRoomMap().values() != null) {
				Iterator<Room> rooms = server.getRoomMap().values().iterator();
				while (rooms.hasNext()) {
					Room room = rooms.next();
					// find the room whose owner is this client
					if (!room.getRoomIdentity().equals("MainHall")) {
						if (room.getOwner().equals(identity)) {
								// set owner to null
							server.getRoomMap().get(room.getRoomIdentity()).setOwner("");
							
							// decide if to delete the room
							boolean isEmpty = true;
							if (server.getRoomMap().get(room.getRoomIdentity())
									.getClients() != null) {
								Iterator<Entry<String, ClientThread>> clients = server
										.getRoomMap().get(room.getRoomIdentity())
										.getClients().entrySet().iterator();
								while (clients.hasNext()) {
									if (!clients.next().getKey().equals(identity)) {
										isEmpty = false;
									}
								}
							}
							// if the room will be empty without the owner then
							// delete it
							if (isEmpty) {
								server.getRoomMap().remove(room.getRoomIdentity());
							}
						}
					}
				}
			}
		}

	}

	public void sendMessage(JSONObject sentJSON) {
		try {
			out.writeUTF(sentJSON.toJSONString());
			out.flush();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public String getIdentity() {
		return identity;
	}

	public void setIdentity(String identity) {
		this.identity = identity;
	}

	public Room getRoom() {
		return room;
	}

	public void setRoom(Room room) {
		this.room = room;
	}

	public Socket getSocket() {
		return socket;
	}

	public void setSocket(Socket socket) {
		this.socket = socket;
	}

}
