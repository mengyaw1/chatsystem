package com.swen90015.tcp.server;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.json.simple.JSONObject;

public class EncodingServer {

	// generate JSONObject to respond to identity change request
	@SuppressWarnings("unchecked")
	public JSONObject identityChange(JSONObject object, String formerIdentity,
			boolean used) {
		String identity = (String) object.get("identity");
		JSONObject newIdentity = new JSONObject();
		newIdentity.put("type", "newidentity");
		newIdentity.put("former", formerIdentity);
		// verify if the identity is valid expression
		String reg = "[A-Za-z][0-9A-Za-z]{2,15}";
		Pattern pattern = Pattern.compile(reg);
		Matcher matcher = pattern.matcher((String) object.get("identity"));
		boolean isValid = matcher.matches();
		// decide to approve the identity change request or not
		if (!used && isValid) {
			newIdentity.put("identity", identity);
		} else {
			newIdentity.put("identity", formerIdentity);
		}

		return newIdentity;
	}

	// generate JSONObject to respond to room change request
	@SuppressWarnings("unchecked")
	public JSONObject roomChange(JSONObject object, String identity, Room room, boolean isValid) {
		JSONObject roomChange = new JSONObject();
		roomChange.put("type", "roomchange");
		roomChange.put("identity", identity);
		roomChange.put("former", room.getRoomIdentity());
		//decide to approve the room change or not
		if(isValid){
			roomChange.put("roomid", object.get("roomid"));
		}else{
			roomChange.put("roomid", room.getRoomIdentity());
		}
		return roomChange;
	}

}
