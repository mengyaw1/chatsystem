package com.swen90015.tcp.client;

import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.Socket;
import java.net.UnknownHostException;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;

import javax.net.ssl.KeyManager;
import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.TrustManagerFactory;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

public class ServerThread implements Runnable {
	private String identity = "";
	private String room = "";
	private Socket socket = null;
	private String createRoom = "";
	boolean disconnect = false;
	private Integer port;
	private String remoteServer;
	
	public ServerThread(String remoteServer, Integer port){
		this.remoteServer = remoteServer;
		this.port = port;
	}

	public void run() {
		try {
			// hard code the values of the JVM variables as follows:
//			if (new File("certificates/client/clienttruststore.jks").isFile()) {
//				System.setProperty("javax.net.ssl.trustStore", "certificates/client/clienttruststore.jks");
//				System.setProperty("javax.net.ssl.trustStorePassword","password");
//			}
			
			//Sun SSL
			char[] passphrase = "password".toCharArray();
		    KeyStore keystore = KeyStore.getInstance("JKS");
		    InputStream keystoreInputStream = getClass().getResourceAsStream("/clientkeystore.jks");
		    keystore.load(keystoreInputStream, passphrase);
//		    keystore.load(new FileInputStream("/clientkeystore.jks"), passphrase);
		    
		    KeyStore trustKeyStore = KeyStore.getInstance("JKS");
		    InputStream trustKeyStoreInputStream = getClass().getResourceAsStream("/clienttruststore.jks");
		    trustKeyStore.load(trustKeyStoreInputStream, passphrase);
//		    trustKeyStore.load(new FileInputStream("/clienttruststore.jks"), passphrase);
		    
		    KeyManagerFactory kmf = KeyManagerFactory.getInstance("SunX509");
		    kmf.init(keystore,passphrase);
		    KeyManager[] keyManagers = kmf.getKeyManagers();

		    TrustManagerFactory tmf = TrustManagerFactory.getInstance("SunX509");
		    tmf.init(trustKeyStore);
		    TrustManager[] trustManagers = tmf.getTrustManagers();

		    SSLContext context = SSLContext.getInstance("TLS");
		    context.init(keyManagers, trustManagers, null);
			
			// Create SSL socket factory, which creates SSLSocket instances
		    SSLSocketFactory factory = context.getSocketFactory();
//			SSLSocketFactory factory= (SSLSocketFactory) SSLSocketFactory.getDefault();
			
			// Use the factory to instantiate SSLSocket
			socket = factory.createSocket(remoteServer, port);

			// Receiving command line input from client using a new thread
			Thread commandLine = new Thread(new CLThread(this));
			commandLine.start();

			// Preparing receiving streams from server
			DataInputStream in = new DataInputStream(socket.getInputStream());
			DecodingClient decodingClient = new DecodingClient();

			while (true) {
				String msg = in.readUTF();
				JSONParser parser = new JSONParser();
				JSONObject fromServer = (JSONObject) parser.parse(msg);
				String type = decodingClient.decoding(fromServer);
				switch (type) {
				case "newidentity":
					identityChange(decodingClient.getNewIdentity(fromServer));
					printPrefix();
					break;
				case "roomchange":
					// disconnect room change
					if (fromServer.get("roomid").equals("")) {
						roomChange(decodingClient.getRoomChange(fromServer));
						disconnect = true;
						break;
					}
					// normal room change
					else {
						roomChange(decodingClient.getRoomChange(fromServer));
						printPrefix();
						break;
					}
				case "roomcontent":
					decodingClient.getRoomContent(fromServer);
					printPrefix();
					break;

				case "roomlist":
					decodingClient.getRoomList(fromServer, createRoom);
					// reset createRoom to null
					createRoom = "";
					printPrefix();
					break;
				case "message":
					System.out.println(fromServer.get("identity") + ": "
							+ fromServer.get("content"));
					break;
				}

				if (disconnect == true) {
					// server.stop();
					break;
				}
			}

			in.close();
			socket.close();
		} catch (UnknownHostException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (KeyStoreException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (CertificateException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (KeyManagementException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (UnrecoverableKeyException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void identityChange(JSONObject object) {
		if (object.get("former").equals(identity)) {
			setIdentity((String) object.get("identity"));
		}
	}

	public void roomChange(JSONObject object) {
		if (object.get("identity").equals(identity)) {
			setRoom((String) object.get("roomid"));
		}

	}

	public void printPrefix() {
		System.out.println("[" + room + "] " + identity + ">");
	}

	public Socket getSocket() {
		return socket;
	}

	public String getIdentity() {
		return identity;
	}

	public void setIdentity(String identity) {
		this.identity = identity;
	}

	public String getRoom() {
		return room;
	}

	public void setRoom(String room) {
		this.room = room;
	}

	public String getCreateRoom() {
		return createRoom;
	}

	public void setCreateRoom(String createRoom) {
		this.createRoom = createRoom;
	}

}
