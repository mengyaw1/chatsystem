package com.swen90015.tcp.client;

import java.util.StringTokenizer;
import org.json.simple.JSONObject;

public class EncodingClient {
	
	@SuppressWarnings("unchecked")
	public JSONObject encoding(String msgClient) {
		// json object to return
		JSONObject objClient = new JSONObject();

		// put data in JSON as message
		StringTokenizer wordClient = new StringTokenizer(msgClient, " ");
		String firstWordClient = wordClient.nextToken();

		if (firstWordClient.toCharArray()[0] == '#') {
			// command type message
			objClient.put("type", firstWordClient.substring(1));
			
			switch (firstWordClient.substring(1)) {
			case "identitychange":
				objClient.put("identity", wordClient.nextToken());
				break;
			case "join":
				objClient.put("roomid", wordClient.nextToken());
				break;
			case "who":
				objClient.put("roomid", wordClient.nextToken());
				break;
			case "list":
				break;
			case "createroom":
				objClient.put("roomid", wordClient.nextToken());
				break;
			case "kick":
				objClient.put("roomid", wordClient.nextToken());
				objClient.put("time", wordClient.nextToken());
				objClient.put("identity", wordClient.nextToken());
				break;
			case "delete":
				objClient.put("roomid", wordClient.nextToken());
				break;
			case "login":
				objClient.put("identity",  wordClient.nextToken());
				objClient.put("password",  wordClient.nextToken());
				break;
			case "register":
				objClient.put("identity",  wordClient.nextToken());
				objClient.put("password",  wordClient.nextToken());
				break;
			case "quit":
				break;				
			}
		} else {
			// normal message
			objClient.put("type", "message");
			objClient.put("content", msgClient);
		}
		
		return objClient;

	}

}
