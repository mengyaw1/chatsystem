package com.swen90015.tcp.client;

import org.kohsuke.args4j.Argument;
import org.kohsuke.args4j.Option;

public class Arg {

	@Argument
	public String romoteServer;
	@Option(name = "-p", usage = "Determine the port")
	public Integer port = 4444;
}
