package com.swen90015.tcp.client;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.EOFException;
import java.io.IOException;
import java.net.Socket;
import java.util.Scanner;

import org.json.simple.JSONObject;
import org.json.simple.parser.ParseException;

public class CLThread implements Runnable {
	private ServerThread serverThread;

	public CLThread(ServerThread serverThread) {
		this.serverThread = serverThread;
	}

	@Override
	public void run() {
		try {
			DataInputStream in = new DataInputStream(serverThread.getSocket()
					.getInputStream());
			DataOutputStream out = new DataOutputStream(serverThread
					.getSocket().getOutputStream());

			// Reading from console
			Scanner cmdin = new Scanner(System.in);
			// To encode messages sent to server
			EncodingClient encodingClient = new EncodingClient();
			while (true) {
				String msgClient = cmdin.nextLine();
				JSONObject objClient = new JSONObject();

				// put message in a json object
				try{
					objClient = encodingClient.encoding(msgClient);
				}catch(Exception e){
					continue;
				}

				// record the room id for 
				if(objClient.get("type").equals("createroom")){
					serverThread.setCreateRoom((String)objClient.get("roomid"));
				}
				// forcing TCP to send data immediately
				out.writeUTF(objClient.toJSONString());
				out.flush();

				if (objClient.get("type").equals("quit"))
					break;
			}

			cmdin.close();
		} catch (IOException e) {
			e.printStackTrace();
		}

		// A thread finishes if run method finishes
	}

}
